import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Container from "@material-ui/core/Container";

const useStyle = makeStyles( theme => ({
    footer: {
        textAlign: 'center',
        backgroundColor: '#EEEFF1',
        padding: '60px 0',
    },
    chip: {
        backgroundColor: '#164A78',
        color: '#FFF',
        margin: '0 10px',
        padding: '0 15px',
    },
    logo: {
        width: '100%',
        maxWidth: '450px',
        marginTop: '40px',
    },
    links: {
        marginTop: '40px',
    }
}));

const Footer = () => {
    const classes = useStyle();

    return (
        <footer className={classes.footer}>
            <div>
                <Chip className={classes.chip} label="Folge uns"/>
                <Chip className={classes.chip} label="Teilen"/>
            </div>
            <img className={classes.logo} src="assets/Logo_Projekt_01.svg" />
            <div className={classes.links}>
                <a class="footer-link" href="#">Über uns</a>
                <a class="footer-link" href="#">Copyright</a>
                <a class="footer-link" href="#">Disclaimer</a>
                <a class="footer-link" href="#">Privacy Statement</a>
                <a class="footer-link" href="#">Impressum</a>
                <a class="footer-link" href="#">Terms of Use</a>
                <a class="footer-link" href="#">Cookie-Präferenzen</a>
            </div>
        </footer>
    );
}

export default Footer;