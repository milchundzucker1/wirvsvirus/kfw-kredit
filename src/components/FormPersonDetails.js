import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  margin: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const FormPersonDetails = props => {
  const classes = useStyles();
  const { formData, handleChange } = props;

  return (
    <>
      <TextField
        id="standard-basic"
        label="Nachname"
        name="lastName"
        value={formData.person.lastName}
        onChange={handleChange('person', 'lastName')}
        fullWidth
        variant="outlined"
        className={classes.margin}
        required
        error
      />
      <TextField
        id="standard-basic"
        label="Vorname"
        name="firstName"
        value={formData.person.firstName}
        onChange={handleChange('person', 'firstName')}
        fullWidth
        variant="outlined"
        className={classes.margin}
        required
      />
    </>
  );
};

export default FormPersonDetails;
