import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  margin: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const FormPersonAddress = props => {
  const classes = useStyles();
  const { formData, handleChange } = props;

  return (
    <>
      <TextField
        id="standard-basic"
        label="Straße"
        name="street"
        value={formData.person.street}
        onChange={handleChange('person', 'street')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
      <TextField
        id="standard-basic"
        label="PLZ"
        name="zipCode"
        value={formData.person.zipCode}
        onChange={handleChange('person', 'zipCode')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
      <TextField
        id="standard-basic"
        label="Ort"
        name="city"
        value={formData.person.city}
        onChange={handleChange('person', 'city')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
    </>
  );
};

export default FormPersonAddress;
