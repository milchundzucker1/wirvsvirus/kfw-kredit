import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import FormPersonDetails from './FormPersonDetails';
import FormPersonAddress from './FormPersonAddress';
import FormCompanyDetails from './FormCompanyDetails';
import FormSummary from './FormSummary';

const useStyles = makeStyles(theme => ({
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));
const person = {
  lastName: '',
  firstName: '',
  street: '',
  zipCode: '',
  city: '',
};
const company = {
  name: '',
  industry: '',
  numberOfEmployees: '',
  revenue: '',
};

const Form = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [formData, setFormData] = useState({
    person,
    company,
  });
  const steps = ['Das bin ich', 'Hier wohne ich', 'Das ist mein Unternehmen'];

  const handleChange = (object, key) => event => {
    const newObject = { ...formData[object], [key]: event.target.value };
    setFormData({ ...formData, [object]: newObject });
  };

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setFormData({ person, company });
    setActiveStep(0);
  };

  const getStepContent = step => {
    switch (step) {
      case 0:
        return (
          <FormPersonDetails formData={formData} handleChange={handleChange} />
        );
      case 1:
        return (
          <FormPersonAddress formData={formData} handleChange={handleChange} />
        );
      case 2:
        return (
          <FormCompanyDetails formData={formData} handleChange={handleChange} />
        );
      default:
        return 'Unbekannter Schritt';
    }
  };

  return (
    <form>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(index)}
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Zurück
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Absenden' : 'Weiter'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <FormSummary formData={formData} />
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
        </Paper>
      )}
    </form>
  );
};

export default Form;
