import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import {Paper} from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

const useStyle = makeStyles( theme => ({
  centered: {
    textAlign: 'center',
  }
}));

const FaqArea = () => {
  const classes = useStyle();

  return (
    <Container className={classes.centered}>
      <h2>Wonach viele suchen:</h2>
      <span>Die drei häufigsten Fragen in den Bereichen</span>

      <Grid container spacing={10}>
        <Grid item xs>
          <Paper>
            <Typography variant="h4">Kinderbetreuung</Typography>
            <List>
              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>
            </List>
          </Paper>
        </Grid>

        <Grid item xs>
          <Paper>
            <Typography variant="h4">Wohnung</Typography>
            <List>
              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>
            </List>
          </Paper>
        </Grid>

        <Grid item xs>
          <Paper>
            <Typography variant="h4">Arbeitnehmer</Typography>
            <List>
              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>

              <ListItem>
                <Link>
                  <ListItemText primary="Welche Mittel bekomme ich, um meine Kinder zu betreuen?"/>
                </Link>
              </ListItem>
            </List>
          </Paper>
        </Grid>

      </Grid>
    </Container>
  );
};

export default FaqArea;
