import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';
import SearchIcon from '@material-ui/icons/Search';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles( theme => ({
  toolbar: {
    height: theme.spacing(12),
    justifyContent: 'flex-end',
  },
  faq: {
    marginRight: theme.spacing(0.5),
    fontSize: '1.75rem',
    textTransform: 'initial'
  },
  logo: {
    boxSizing: 'border-box',
    marginRight: 'auto',
    height: '100%',
    padding: theme.spacing(2),
    paddingLeft: theme.spacing(4),
  },
}));

const Header = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => setAnchorEl(event.currentTarget);

  const handleClose = () => setAnchorEl(null);

  const classes = useStyles();

  return (
    <header>
      <AppBar position="fixed" color="default">
        <Toolbar className={classes.toolbar}>
          <img src={'logo.svg'} alt={"Logo von Corona Finanzhilfe"} className={classes.logo}/>

          <Button href="#" size="large" color="primary" className={classes.faq}>
            FAQs
          </Button>

          <IconButton color="primary">
            <AnnouncementIcon fontSize="large"/>
          </IconButton>

          <IconButton color="primary">
            <SearchIcon fontSize="large"/>
          </IconButton>

          <IconButton color="primary">
            <PersonOutlineIcon fontSize="large"/>
          </IconButton>
        </Toolbar>
      </AppBar>
    </header>
  );
};

export default Header;
