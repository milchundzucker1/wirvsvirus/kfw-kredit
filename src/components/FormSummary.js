import React from 'react';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

const FormSummary = props => {
  const { person, company } = props.formData;

  console.log(person);

  return (
    <>
      <Typography>Zusammenfassung</Typography>
      <List
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            Das bin ich
          </ListSubheader>
        }
      >
        <ListItem>
          <ListItemText primary="Nachname" secondary={person.lastName} />
        </ListItem>
        <ListItem>
          <ListItemText primary="Vorname" secondary={person.firstName} />
        </ListItem>
      </List>
      <List
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            Das ist mein Unternehmen
          </ListSubheader>
        }
      >
        <ListItem>
          <ListItemText primary="TBD" secondary="..." />
        </ListItem>
      </List>
    </>
  );
};

export default FormSummary;
