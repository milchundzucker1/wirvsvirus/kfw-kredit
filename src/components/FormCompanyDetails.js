import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles(theme => ({
  margin: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const FormPersonDetails = props => {
  const classes = useStyles();
  const { formData, handleChange } = props;

  return (
    <>
      <TextField
        id="companyName"
        label="Name"
        name="companyName"
        value={formData.company.companyName}
        onChange={handleChange('company', 'companyName')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
      <TextField
        id="standard-basic"
        label="Branche"
        name="industry"
        value={formData.company.industry}
        onChange={handleChange('company', 'industry')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
      <TextField
        id="standard-basic"
        label="Anzahl Mitarbeiter"
        name="numberOfEmployees"
        value={formData.company.numberOfEmployees}
        onChange={handleChange('company', 'numberOfEmployees')}
        fullWidth
        variant="outlined"
        className={classes.margin}
      />
      <FormControl fullWidth className={classes.margin} variant="outlined">
        <InputLabel htmlFor="revenue">Umsatz</InputLabel>
        <OutlinedInput
          id="revenue"
          value={formData.numberOfEmployees}
          onChange={handleChange('company', 'revenue')}
          startAdornment={<InputAdornment position="start">$</InputAdornment>}
          labelWidth={60}
        />
      </FormControl>
    </>
  );
};

export default FormPersonDetails;
