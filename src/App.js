import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';
import Form from './components/Form';
import WelcomePage from './pages/WelcomePage'
import {createMuiTheme, ThemeProvider} from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#336f91',
      main: '#004B76',
      dark: '#003452',
    },
    secondary: {
      light: '#f8c863',
      main: '#F7BB3D',
      dark: '#ac822a',
    },
  },
  typography: {
    fontFamily: [
      'OpenSans',
      'Arial',
      'sans-serif',
    ].join(','),
  },
});

const App = () => {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Header />
        <Router>
          <Switch>
            <Route path="/">
              <WelcomePage />
            </Route>
          </Switch>
        </Router>
        <Footer />
      </ThemeProvider>
    </div>
  );
};

export default App;
