import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Card, CardContent } from '@material-ui/core';
import CardHeader from '@material-ui/core/CardHeader';
import PersonIcon from '@material-ui/icons/Person';
import BusinessIcon from '@material-ui/icons/Business';
import Chip from '@material-ui/core/Chip';
import FaqArea from '../components/FaqArea';

const useStyle = makeStyles(theme => ({
  centered: {
    textAlign: 'center',
  },
}));

const WelcomePage = () => {
  const classes = useStyle();

  return (
    <Container className={classes.centered}>
      <h1>#WirVsVirus: Wir helfen Deutschland</h1>
      <p>
        Du bist auf der Plattform, die dir hilft finanzielle Hilfe zu bekommen.
        <br />
        Ob Kinderbetreuung, Kurzarbeit, Wohnsituation:
        <br />
        Wir sagen dir, wo du Hilfe bekommst.
      </p>

      <p>
        Wenn du noch nicht weißt, was du genau suchst, hilft dir unser Bot oder
        klick dich durch die Fragen:
      </p>

      <Card>
        <CardHeader title="Wer bist du?" />
        <CardContent>
          <Box display="flex" flexDirection={'row'} justifyContent={'center'}>
            <Box display="flex" flexDirection="column" alignItems="center">
              <PersonIcon />
              Privatperson
            </Box>
            <span>oder</span>
            <Box display="flex" flexDirection="column" alignItems="center">
              <BusinessIcon />
              Unternehmen
            </Box>
          </Box>
        </CardContent>
      </Card>

      <p>Du weißt schon, welchen Antrag du suchst?</p>
      <Chip label="Zeig mir alle Kategorien" />
      <FaqArea />
    </Container>
  );
};

export default WelcomePage;
